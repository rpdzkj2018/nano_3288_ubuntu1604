#!/bin/bash

MAKE_THEARD=`cat /proc/cpuinfo| grep "processor"| wc -l`
RESULT="Image-rk3288-ubuntu"

function creat_result_dir()
{
        if [ ! -d $RESULT ];
                then
                {
                        mkdir Image-rk3288-ubuntu
                        chmod -R 777 Image-rk3288-ubuntu
                }
        fi
}

function u-boot_build()
{
	pushd u-boot/
        make rk3288_secure_defconfig
        ./mkv7.sh
        if [ $? -eq 0 ]; then
                echo "Build uboot ok!"
        else
                echo "Build uboot failed!"
                exit 1
        fi
        popd
        
	cp u-boot/rk3288_loader_v1.06.236.bin $RESULT/RKLoader.bin
	cp u-boot/trust.img $RESULT/trust.img
	cp u-boot/uboot.img $RESULT/uboot.img
}
function kernel_build()
{
        pushd kernel/
                #make ARCH=arm rockchip_linux_defconfig
                #if [ -f "logo.png" ]; then
                #        {
                #                rm drivers/video/logo/logo_linux_clut224.*
                #                pngtopnm logo.png > linuxlogo.pnm
                #                pnmquant 224 linuxlogo.pnm > linuxlogo224.pnm
                #                pnmtoplainpnm linuxlogo224.pnm > linuxlogo224.ppm
                #                mv linuxlogo224.ppm drivers/video/logo/logo_linux_clut224.ppm
                #        }
                #elif [ -f "logo.bmp" ]; then
                #        {
                #                rm drivers/video/logo/logo_linux_clut224.*
                #                bmptoppm logo.bmp > linuxlogo.ppm
                #                ppmquant 224 linuxlogo.ppm > linuxlogo224.ppm
                #                pnmnoraw linuxlogo224.ppm > drivers/video/logo/logo_linux_clut224.ppm
                #        }
                #fi
                make ARCH=arm rp-rk3288.img -j8
                if [ $? -eq 0 ]; then
                        echo "Build kernel ok!"
                else
                        echo "Build kernel failed!"
                        exit 1
                fi
        popd

        rm $RESULT/linux-boot.img
        make -C initrd
        mkbootimg --kernel kernel/arch/arm/boot/zImage --ramdisk initrd.img --second kernel/resource.img  -o linux-boot.img
        rm initrd.img
        mv linux-boot.img $RESULT
}

function ubuntu_core_build() 
{
		cp linux-rootfs-core.img $RESULT/linux-rootfs-core.img
}
function ubuntu_build() 
{
	cp linux-rootfs.img $RESULT/linux-rootfs.img
}

function kernel_clean() 
{
        echo make clean kernel
	pushd kernel/
	make clean
	popd
	rm $RESULT/linux-boot.img
}
function uboot_clean() 
{
	echo make clean u-boot
	pushd u-boot/
	make distclean
	popd
	rm $RESULT/RKLoader.bin
}
function ubuntu_clean() 
{
	rm $RESULT/linux-rootfs.img
}
function ubuntu_core_clean() 
{
	rm $RESULT/linux-rootfs-core.img
}
function result_clean() 
{
	rm -rf $RESULT
}
function null() 
{
	cp make.sh make_back.sh
}

creat_result_dir
if [ $1 == "clean" ]
	then
	{
		if [ ! -n $2 ]
			then
			{
				uboot_clean
				kernel_clean
				ubuntu_core_clean
				ubuntu_clean
				result_clean
				echo clean Img oK
			}
		elif [ $2 == "u-boot" -o $2 == "u-boot/" ]
			then
			{
				uboot_clean
				uboot_clean
			}
		elif [ $2 == "kernel" -o $2 == "kernel/" ]
			then
			{
				kernel_clean
			}
		elif [ $2 == "ubuntu" -o $2 == "ubuntu/" ]
			then
			{
				ubuntu_clean
			}
		elif [ $2 == "ubuntu_core" -o $2 == "ubuntu_core/" -o $2 == "ubuntu-core" -o $2 == "ubuntu-core/" ]
			then
			{
				ubuntu_core_clean
			}
		else
			{
				uboot_clean
				kernel_clean
				ubuntu_core_clean
				ubuntu_clean
				result_clean
				echo clean Img oK
			}
		fi
	}
elif [ $1 == "u-boot" -o $1 == "u-boot/" ]
	then
	{
		u-boot_build
	}
elif [ $1 == "kernel" -o $1 == "kernel/" ]
	then
	{
		kernel_build
	}
elif [ $1 == "ubuntu_core" -o $1 == "ubuntu_core/" -o $1 == "ubuntu-core" -o $1 == "ubuntu-core/" ]
	then
	{
		ubuntu_core_build
		./rpupdate.sh core
	}
elif [ $1 == "ubuntu" -o $1 == "ubuntu/" ]
	then
	{
		#null
		ubuntu_build
		./rpupdate.sh
	}
else
	{
		u-boot_build
		kernel_build
		ubuntu_core_build
		ubuntu_build
		cp parameter $RESULT
		./rpupdate.sh
	}
fi


