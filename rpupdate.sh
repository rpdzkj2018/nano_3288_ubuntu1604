#!/bin/bash

LINUX_UPDATE="linux_update/rockdev/Image/"
if [ ! -d $LINUX_UPDATE ]
        then
        {
                cd linux_update/rockdev/
                mkdir Image
                chmod -R 777 Image
                cd -
        }
fi
BOARD_DIR="Image-rk3288-ubuntu"
cp u-boot/rk3288_loader_v1.06.236.bin 		$LINUX_UPDATE/RKLoader.bin
cp  $LINUX_UPDATE/RKLoader.bin			linux_update/rockdev/
cp u-boot/trust.img 				$LINUX_UPDATE
cp u-boot/uboot.img 				$LINUX_UPDATE
cp $BOARD_DIR/parameter		 		$LINUX_UPDATE
cp $BOARD_DIR/linux-boot.img	 		$LINUX_UPDATE
if [ $1 == "core" ]
	then
		cp $BOARD_DIR/linux-rootfs-core.img 			$LINUX_UPDATE/linux-rootfs.img
	else
		cp $BOARD_DIR/linux-rootfs.img 			$LINUX_UPDATE/linux-rootfs.img

fi
rm $BOARD_DIR/ubuntu*

pushd linux_update/rockdev
. mkupdate.sh
if [ $? -eq 0 ]; then
        echo "update.img make OK!"
else
        echo "update.img make FAILED!"
        exit 1
fi
popd
chmod 777 linux_update/rockdev/update.img
DATE_CURRENT=$(date +%Y%m%d_%H%M)
mv linux_update/rockdev/update.img $BOARD_DIR/ubuntu_"$DATE_CURRENT".img
rm -rf $LINUX_UPDATE/*
